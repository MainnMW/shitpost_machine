import discord
from discord.ext import commands
import requests
import random
import markovify
import json
vk_settings  = {
    'vk_api': '2d8549162d8549162d854916ea2e94f48d22d852d8549164e0924f96e4fc7201fb89897',
}
class Anek(commands.Cog):
    def __init__(self, bot, storage) -> None:
        super().__init__()
        self.bot = bot
        self.anek_url = 'https://api.vk.com/method/wall.get'
        self.storage = storage
    @commands.command()
    async def anek(self, ctx):
        """
        Постинг анекдота категории Б
        """
        anek_number = random.randint(1,2000)
        response = requests.get(self.anek_url, params={
            'owner_id': -45491419,
            'offset': anek_number,
            'extender': 0,
            'count': 1,
            'access_token': vk_settings['vk_api'],
            'v': 5.131
            })
        anek_list = response.json()['response']['items']
        for anek in anek_list:
            print(anek['text'].split(' '))
            await ctx.send(anek['text'])
    @commands.command()
    async def anek_suck_it(self, ctx):
        """
        выкачка анекдотов категории Б
        """
        print(ctx.message.author.name)
        if ctx.message.author.name != 'Mainnmw' and ctx.message.author.name != 'Silent Wind Bell':
            return 
        current = 0
        word = []
        with open('category_b.txt', 'w', encoding="utf-8") as hihi_b:
            
            while current < 5000:
                response = requests.get(self.anek_url, params={
                'owner_id': -45491419,
                'offset': current,
                'extender': 0,
                'count': 100,
                'access_token': vk_settings['vk_api'],
                'v': 5.131
                })
                anek_list = response.json()['response']['items']
                for anek in anek_list:
                    for token in anek['text'].split(' '):
                        word.append(token)
                current = current + 100
        self.storage.hihi_chain = markovify.Text(' '.join(word), state_size=3)
        with open("category_b.json", "w") as outfile:
            json.dump(self.storage.hihi_chain.to_json(), outfile)
        print("its done")
        await ctx.send(self.storage.hihi_chain.make_short_sentence(500))
    @commands.command()
    async def anek_shit(self, ctx):
        self.storage.setup_hihi()
        anek_number = random.randint(500,2000)
        await ctx.send(self.storage.hihi_chain.make_short_sentence(anek_number))