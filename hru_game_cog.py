import discord
from discord.ext import commands
import requests
import random
import markovify
import json
from datetime import datetime

class Le_Pig():
    _image = ""
    _name  = ""
    _id = ""
    _owner = ""
    _speed = 0
    def init(self, name, id, owner, speed):
        self._name = name
        self._id = id
        self._owner = owner 
        self._speed = speed
        pass
    
    @property
    def image(self):
        return self._image

    @image.setter
    def image(self, image):
        self._image = image

    @property
    def speed(self):
        return self._speed

    @speed.setter
    def speed(self, speed):
        self._speed = speed

class User():
    _pigs = []
    id = ''
    balance = 100
    def __init__(self) -> None:
        pass

    def add_pig(self, pig:Le_Pig):
        self._pigs.append(pig)

    def buy(self, name, owner, speed):
        pig_id = hash(datetime.now())
        self.add_pig(Le_Pig(name, pig_id, owner, speed))
    
class UserList():
    users = []
    def add_user(self, user: User):
        """
        Добавляем юзера в 'БД'
        """
        self.users.append(user)

    def edit_user(self, id, user: User):
        """
        Изменить юзера в 'БД'
        """
        pass

    def get_user(self, id):
        user_fn = lambda x: x.id == id
        return filter(user_fn, self.users)[0]

class Le_Pig_Cog(commands.Cog):
    def __init__(self, bot) -> None:
        super().__init__()
        self.bot = bot
        self.UserList = UserList()
    @commands.command()
    async def buy_pig(self, ctx):
        try:
            self.UserList.get_user(id = ctx.message.author.name)
        except:
            ctx.send(f"""Юзер с вашим ником не найден, писать на @Mainnmw""")
    